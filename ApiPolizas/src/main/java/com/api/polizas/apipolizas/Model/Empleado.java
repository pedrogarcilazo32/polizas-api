package com.api.polizas.apipolizas.Model;

import lombok.Data;

@Data
public class Empleado {
    int idEmpleado;
    String nombre;
    String apellido;
    int idPuesto;
    Boolean activo_opc;
    String fecha_reg;
    String puestoNombre;
}
