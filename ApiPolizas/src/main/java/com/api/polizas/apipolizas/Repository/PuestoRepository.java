package com.api.polizas.apipolizas.Repository;

import com.api.polizas.apipolizas.Model.Puesto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import java.sql.Types;
import java.util.List;
import com.fasterxml.jackson.databind.*;
@Repository
public class PuestoRepository implements IPuestoRepository{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    ObjectMapper objectMapper = new ObjectMapper();

    @Override
 /*   public List<Puesto> obtenerPuestos() {
        String SQL = "SelEct * FROM cat_puesto";

        return jdbcTemplate.query(SQL, BeanPropertyRowMapper.newInstance(Puesto.class));
    } */

    public List<Puesto> obtenerPuestos() {
        //objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        String callProcedure = "{ CALL pol_sp_puestos(?, ?) }";
        var puesto = new Puesto();



        String data = null;
        try {
            data = objectMapper.writeValueAsString(puesto);
        } catch (JsonProcessingException e) {
            System.out.println(e);
            throw new RuntimeException(e);
        }
        System.out.println(data);
        System.out.println("Hola");
        return jdbcTemplate.query(callProcedure, new Object[]{2,data},
                new int[]{Types.INTEGER, Types.VARCHAR},
                BeanPropertyRowMapper.newInstance(Puesto.class));
    }

}
