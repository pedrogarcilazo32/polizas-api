package com.api.polizas.apipolizas.Services;

import com.api.polizas.apipolizas.Model.Contenedor;
import com.api.polizas.apipolizas.Model.Empleado;
import com.api.polizas.apipolizas.Model.Inventario;
import com.api.polizas.apipolizas.Repository.IInventarioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InventarioServices implements IInventarioServices{
    private static final Logger logger = LoggerFactory.getLogger(EmpleadoServices.class);
    @Autowired
    private IInventarioRepository iInventarioRepository;
    @Override
    public Contenedor obtenerInventariosActivos() {
        logger.info("INICIA obtenerInventariosActivos");
        Contenedor aux = new Contenedor();
        List<Inventario> list;
        try {
            list=iInventarioRepository.obtenerInventariosActivos();
            aux.setEstatus(true);
            aux.setMensaje("OK");
        }
        catch (Exception e){
            logger.error("ERROR obtenerInventariosActivos",e);
            list=null;
            aux.setEstatus(false);
            aux.setMensaje("No se pudo obtener los inventarios activos");
            throw e;

        }
        logger.info("FIN obtenerInventariosActivos");
        aux.setInventarios(list);
        return aux;
    }
}
