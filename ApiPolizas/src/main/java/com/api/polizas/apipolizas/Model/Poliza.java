package com.api.polizas.apipolizas.Model;

import lombok.Data;

@Data
public class Poliza {
    int idPoliza;
    int idEmpleado;
    String sku_cod;
    int cantidad;
    Boolean activo_opc;
    String fecha_reg;
    String nombreEmpleado;
    String busqueda;
    int total_paginas;
    int pagina;
    int paginado;
}
