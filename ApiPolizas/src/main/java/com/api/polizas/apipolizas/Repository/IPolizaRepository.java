package com.api.polizas.apipolizas.Repository;

import com.api.polizas.apipolizas.Model.Poliza;

import java.util.List;

public interface IPolizaRepository {
    public Boolean generarPoliza(Poliza data);
    public List<Poliza> obtenerPolizasPaginado(Poliza data);
    public List<Poliza> obtenerPolizas();
    public Boolean desactivarPoliza(Poliza data);
}
