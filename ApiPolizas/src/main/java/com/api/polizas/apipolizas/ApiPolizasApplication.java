package com.api.polizas.apipolizas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiPolizasApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiPolizasApplication.class, args);
    }

}
