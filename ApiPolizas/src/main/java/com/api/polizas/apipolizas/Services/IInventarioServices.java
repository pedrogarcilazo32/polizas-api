package com.api.polizas.apipolizas.Services;

import com.api.polizas.apipolizas.Model.Contenedor;

public interface IInventarioServices {
    public Contenedor obtenerInventariosActivos();
}
