package com.api.polizas.apipolizas.Services;

import com.api.polizas.apipolizas.Model.Contenedor;
import com.api.polizas.apipolizas.Model.Empleado;
import com.api.polizas.apipolizas.Model.Poliza;
import com.api.polizas.apipolizas.Repository.IPolizaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PolizaServices implements  IPolizaServices{
    private static final Logger logger = LoggerFactory.getLogger(EmpleadoServices.class);
    @Autowired
    private IPolizaRepository iPolizaRepository;
    @Override
    public Contenedor generarPoliza(Poliza data) {
        logger.info("INICIA generarPoliza");
        Contenedor aux = new Contenedor();
        try {
            if(iPolizaRepository.generarPoliza(data)){
                aux.setEstatus(true);
                aux.setMensaje("OK");
            }
            else{
                aux.setEstatus(false);
                aux.setMensaje("No se pudo generar la poliza");
            }

        }
        catch (Exception e){
            logger.error("ERROR generarPoliza",e);

            aux.setEstatus(false);
            aux.setMensaje("No se pudo generar la poliza");
            throw e;

        }
        logger.info("FIN generarPoliza");
        return aux;
    }
    @Override
    public Contenedor obtenerPolizas() {
        logger.info("INICIA obtenerPolizas");
        Contenedor aux = new Contenedor();
        List<Poliza> list ;
        try {
            list=iPolizaRepository.obtenerPolizas();
            aux.setEstatus(true);
            aux.setMensaje("OK");
        }
        catch (Exception e){
            logger.error("ERROR obtenerPolizas",e);
            aux.setEstatus(false);
            aux.setMensaje("No se pudo obtener las polizas");
            throw e;

        }
        aux.setPolizas(list);
        logger.info("FIN obtenerPolizas");
        return aux;
    }
    @Override
    public Contenedor obtenerPolizasPaginado(Poliza data) {
        logger.info("INICIA obtenerPolizasPaginado");
        Contenedor aux = new Contenedor();
        List<Poliza> list ;
        try {
           list=iPolizaRepository.obtenerPolizasPaginado((data));
            aux.setEstatus(true);
            aux.setMensaje("OK");
        }
        catch (Exception e){
            logger.error("ERROR obtenerPolizasPaginado",e);
            aux.setEstatus(false);
            aux.setMensaje("Ha ocurrido un error al consultar el paginado de  pólizas");
            throw e;

        }
        aux.setPolizas(list);
        logger.info("FIN obtenerPolizasPaginado");
        return aux;
    }
    @Override
    public Contenedor desactivarPoliza(int id) {
        Poliza data = new Poliza();
        data.setIdPoliza(id);
        logger.info("INICIA desactivarPoliza");
        Contenedor aux = new Contenedor();
        try {
            if(iPolizaRepository.desactivarPoliza(data)){
                aux.setEstatus(true);
                aux.setMensaje("OK");
            }
            else{
                aux.setEstatus(false);
                aux.setMensaje("Ha ocurrido un error al intentar eliminar la póliza.");
            }

        }
        catch (Exception e){
            logger.error("ERROR desactivarPoliza",e);

            aux.setEstatus(false);
            aux.setMensaje("Ha ocurrido un error al intentar eliminar la póliza.");
            throw e;

        }
        logger.info("FIN desactivarPoliza");
        return aux;
    }
}
