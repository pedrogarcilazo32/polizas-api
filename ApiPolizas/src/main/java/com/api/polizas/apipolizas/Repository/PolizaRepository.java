package com.api.polizas.apipolizas.Repository;

import com.api.polizas.apipolizas.Model.Contenedor;
import com.api.polizas.apipolizas.Model.Empleado;
import com.api.polizas.apipolizas.Model.Poliza;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class PolizaRepository implements  IPolizaRepository{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    ObjectMapper objectMapper = new ObjectMapper();
    @Override
    public Boolean generarPoliza(Poliza data) {
        //objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        String callProcedure = "{ CALL pol_sp_poliza(?, ?) }";
        String obj = "";
        try {
            obj = objectMapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            System.out.println(e);
            obj="";
            throw new RuntimeException(e);
        }

        var result= jdbcTemplate.queryForObject(callProcedure, new Object[]{1,obj},
                new int[]{Types.INTEGER, Types.VARCHAR},
                BeanPropertyRowMapper.newInstance(Contenedor.class));
        boolean estatus = result.getEstatus();
        return  estatus;
    }
    public List<Poliza> obtenerPolizas() {
        //objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        String callProcedure = "{ CALL pol_sp_poliza(?, ?) }";

        return jdbcTemplate.query(callProcedure, new Object[]{2,""},
                new int[]{Types.INTEGER, Types.VARCHAR},
                BeanPropertyRowMapper.newInstance(Poliza.class));
    }
    public List<Poliza> obtenerPolizasPaginado(Poliza data) {
        //objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        String callProcedure = "{ CALL pol_sp_poliza(?, ?) }";
        String obj = "";
        try {
            obj = objectMapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            System.out.println(e);
            obj="";
            throw new RuntimeException(e);
        }
        return jdbcTemplate.query(callProcedure, new Object[]{5,obj},
                new int[]{Types.INTEGER, Types.VARCHAR},
                BeanPropertyRowMapper.newInstance(Poliza.class));
    }
    public Boolean desactivarPoliza(Poliza data) {
        //objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        String callProcedure = "{ CALL pol_sp_poliza(?, ?) }";
        String obj = "";
        try {
            obj = objectMapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            System.out.println(e);
            obj="";
            throw new RuntimeException(e);
        }

        var result= jdbcTemplate.queryForObject(callProcedure, new Object[]{4,obj},
                new int[]{Types.INTEGER, Types.VARCHAR},
                BeanPropertyRowMapper.newInstance(Contenedor.class));
        boolean estatus = result.getEstatus();
        return  estatus;
    }
}
