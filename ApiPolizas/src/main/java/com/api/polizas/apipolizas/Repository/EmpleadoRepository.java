package com.api.polizas.apipolizas.Repository;
import com.api.polizas.apipolizas.Model.Contenedor;
import com.api.polizas.apipolizas.Model.Empleado;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import java.sql.Types;
import java.util.List;
@Repository
public class EmpleadoRepository implements IEmpleadoRepository{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public List<Empleado> obtenerEmpleadosActivos() {
        //objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        String callProcedure = "{ CALL pol_sp_empleados(?, ?) }";
        return jdbcTemplate.query(callProcedure, new Object[]{5,""},
                new int[]{Types.INTEGER, Types.VARCHAR},
                BeanPropertyRowMapper.newInstance(Empleado.class));
    }
    @Override
    public List<Empleado> obtenerEmpleadoPorId(Empleado data) {
        //objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        String callProcedure = "{ CALL pol_sp_empleados(?, ?) }";
        String obj = "";
        try {
            obj = objectMapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            System.out.println(e);
            obj="";
            throw new RuntimeException(e);
        }

        return jdbcTemplate.query(callProcedure, new Object[]{6,obj},
                new int[]{Types.INTEGER, Types.VARCHAR},
                BeanPropertyRowMapper.newInstance(Empleado.class));
    }
    @Override
    public Boolean actualizarEmpleado(Empleado data) {
        //objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        String callProcedure = "{ CALL pol_sp_empleados(?, ?) }";
        String obj = "";
        try {
            obj = objectMapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            System.out.println(e);
            obj="";
            throw new RuntimeException(e);
        }

        var result= jdbcTemplate.queryForObject(callProcedure, new Object[]{3,obj},
                new int[]{Types.INTEGER, Types.VARCHAR},
                BeanPropertyRowMapper.newInstance(Contenedor.class));
        boolean estatus = result.getEstatus();
        return  estatus;
    }
}
