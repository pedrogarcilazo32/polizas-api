package com.api.polizas.apipolizas.Repository;

import com.api.polizas.apipolizas.Model.Puesto;

import java.util.List;

public interface IPuestoRepository {
    public List<Puesto> obtenerPuestos();

}
