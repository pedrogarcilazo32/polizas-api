package com.api.polizas.apipolizas.Repository;
import com.api.polizas.apipolizas.Model.Inventario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import java.sql.Types;
import java.util.List;
@Repository
public class InventarioRepository implements IInventarioRepository{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Override
    public List<Inventario> obtenerInventariosActivos() {
        String callProcedure = "{ CALL pol_sp_inventario(?, ?) }";
        return jdbcTemplate.query(callProcedure, new Object[]{5,""},
                new int[]{Types.INTEGER, Types.VARCHAR},
                BeanPropertyRowMapper.newInstance(Inventario.class));
    }
}
