package com.api.polizas.apipolizas.Controller;

import com.api.polizas.apipolizas.Model.Puesto;
import com.api.polizas.apipolizas.Services.IPuestoServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/puesto")
@CrossOrigin("*") // para que cualcuier cliente lo consuma

public class PuestoController {
    @Autowired
    private IPuestoServices iPuestoServices;
    @GetMapping("/obtenerPuestos")
    public ResponseEntity<List<Puesto>> obtenerPuestos(){
        var result = iPuestoServices.obtenerPuestos();
        return  new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/test")
    public String test(){

        return "si";
    }



}
