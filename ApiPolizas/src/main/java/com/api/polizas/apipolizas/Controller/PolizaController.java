package com.api.polizas.apipolizas.Controller;

import com.api.polizas.apipolizas.Model.Contenedor;
import com.api.polizas.apipolizas.Model.Poliza;
import com.api.polizas.apipolizas.Services.IPolizaServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/poliza")
@CrossOrigin("*") // para que cualcuier cliente lo consuma
public class PolizaController {
    @Autowired
    private IPolizaServices iPolizaServices;
    @PostMapping("/generarPoliza")
    public ResponseEntity<Contenedor> generarPoliza(@RequestBody Poliza data){
        var result = iPolizaServices.generarPoliza(data);
        return  new ResponseEntity<>(result, HttpStatus.OK);
    }
    @GetMapping("/obtenerPolizas")
    public ResponseEntity<Contenedor> obtenerPolizas(){
        var result = iPolizaServices.obtenerPolizas();
        return  new ResponseEntity<>(result, HttpStatus.OK);
    }
    @PostMapping("/obtenerPolizasPaginado")
    public ResponseEntity<Contenedor> obtenerPolizasPaginado(@RequestBody Poliza data){
        var result = iPolizaServices.obtenerPolizasPaginado(data);
        return  new ResponseEntity<>(result, HttpStatus.OK);
    }
    @PutMapping("/desactivarPoliza/{id}")
    public ResponseEntity<Contenedor> desactivarPoliza(@PathVariable int id){
        var result = iPolizaServices.desactivarPoliza(id);
        return  new ResponseEntity<>(result, HttpStatus.OK);
    }
}
