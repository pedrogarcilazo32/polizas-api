package com.api.polizas.apipolizas.Controller;

import com.api.polizas.apipolizas.Model.Contenedor;
import com.api.polizas.apipolizas.Services.IEmpleadoServices;
import com.api.polizas.apipolizas.Services.IInventarioServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/inventario")
@CrossOrigin("*") // para que cualcuier cliente lo consuma
public class InventarioController {
    @Autowired
    private IInventarioServices iInventarioServices;
    @GetMapping("/obtenerInventariosActivos")
    public ResponseEntity<Contenedor> obtenerInventariosActivos(){
        var result = iInventarioServices.obtenerInventariosActivos();
        return  new ResponseEntity<>(result, HttpStatus.OK);
    }
}
