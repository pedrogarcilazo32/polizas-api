package com.api.polizas.apipolizas.Controller;

import com.api.polizas.apipolizas.Model.Contenedor;
import com.api.polizas.apipolizas.Model.Empleado;
import com.api.polizas.apipolizas.Services.IEmpleadoServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/empleado")
@CrossOrigin("*") // para que cualcuier cliente lo consuma
public class EmpleadoController {
    @Autowired
    private IEmpleadoServices iEmpleadoServices;
    @GetMapping("/obtenerEmpleadosActivos")
    public ResponseEntity<Contenedor> obtenerEmpleadosActivos(){
        var result = iEmpleadoServices.obtenerEmpleadosActivos();
        return  new ResponseEntity<>(result, HttpStatus.OK);
    }
    @GetMapping("/obtenerEmpleadoPorId/{id}")
    public ResponseEntity<Contenedor> obtenerEmpleadoPorId(@PathVariable int id){
        var result = iEmpleadoServices.obtenerEmpleadoPorId(id);
        return  new ResponseEntity<>(result, HttpStatus.OK);
    }
    @PostMapping("/actualizarEmpleado")
    public ResponseEntity<Contenedor> actualizarEmpleado(@RequestBody Empleado data){
        var result = iEmpleadoServices.actualizarEmpleado(data);
        return  new ResponseEntity<>(result, HttpStatus.OK);
    }
}
