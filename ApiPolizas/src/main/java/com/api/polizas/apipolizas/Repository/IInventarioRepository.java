package com.api.polizas.apipolizas.Repository;

import com.api.polizas.apipolizas.Model.Inventario;

import java.util.List;

public interface IInventarioRepository {
    public List<Inventario> obtenerInventariosActivos();
}
