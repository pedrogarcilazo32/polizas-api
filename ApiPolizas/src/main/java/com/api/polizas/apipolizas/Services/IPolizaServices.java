package com.api.polizas.apipolizas.Services;

import com.api.polizas.apipolizas.Model.Contenedor;
import com.api.polizas.apipolizas.Model.Poliza;

import java.util.List;

public interface IPolizaServices {
    public Contenedor generarPoliza(Poliza data);
    public Contenedor obtenerPolizas();
    public Contenedor obtenerPolizasPaginado(Poliza data);
    public  Contenedor desactivarPoliza(int id);
}
