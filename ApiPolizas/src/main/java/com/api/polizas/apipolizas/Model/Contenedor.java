package com.api.polizas.apipolizas.Model;

import lombok.Data;

import java.util.List;

@Data
public class Contenedor {

String mensaje;
Boolean estatus;
List<Empleado> empleados;
List<Puesto> puestos;
List<Inventario> inventarios;
List<Poliza> polizas;
}
