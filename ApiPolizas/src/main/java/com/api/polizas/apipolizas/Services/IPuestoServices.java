package com.api.polizas.apipolizas.Services;

import com.api.polizas.apipolizas.Model.Puesto;

import java.util.List;

public interface IPuestoServices {
    public List<Puesto> obtenerPuestos();
}
