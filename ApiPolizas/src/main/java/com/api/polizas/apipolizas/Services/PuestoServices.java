package com.api.polizas.apipolizas.Services;

import com.api.polizas.apipolizas.Model.Puesto;
import com.api.polizas.apipolizas.Repository.IPuestoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PuestoServices   implements IPuestoServices{
    @Autowired
    private IPuestoRepository iPuestoRepository;
    @Override
    public List<Puesto> obtenerPuestos() {
        List<Puesto> list;
        try {
        list=iPuestoRepository.obtenerPuestos();
        }
        catch (Exception e){
            throw e;
        }
        return list;
    }
}
