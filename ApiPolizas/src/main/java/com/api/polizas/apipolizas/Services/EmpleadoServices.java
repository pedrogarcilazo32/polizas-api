package com.api.polizas.apipolizas.Services;

import com.api.polizas.apipolizas.Model.Contenedor;
import com.api.polizas.apipolizas.Model.Empleado;
import com.api.polizas.apipolizas.Model.Puesto;
import com.api.polizas.apipolizas.Repository.IEmpleadoRepository;
import com.api.polizas.apipolizas.Repository.IPuestoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
@Service
public class EmpleadoServices implements IEmpleadoServices{
    private static final Logger logger = LoggerFactory.getLogger(EmpleadoServices.class);

    @Autowired
    private IEmpleadoRepository iEmpleadoRepository;
    @Override
    public Contenedor obtenerEmpleadosActivos() {
        logger.info("INICIA obtenerEmpleadosActivos");
        Contenedor aux = new Contenedor();
        List<Empleado> list;
        try {
            list=iEmpleadoRepository.obtenerEmpleadosActivos();
            aux.setEstatus(true);
            aux.setMensaje("OK");
        }
        catch (Exception e){
            logger.error("ERROR obtenerEmpleadosActivos",e);
            list=null;
            aux.setEstatus(false);
            aux.setMensaje("No se pudo obtener los empleados activos");
            throw e;

        }
        logger.info("FIN obtenerEmpleadosActivos");
        aux.setEmpleados(list);
        return aux;
    }
    @Override
    public Contenedor obtenerEmpleadoPorId(int id) {
        logger.info("INICIA obtenerEmpleadoPorId");
        Contenedor aux = new Contenedor();
        List<Empleado> list;
        try {
            Empleado empAux =new Empleado();
            empAux.setIdEmpleado(id);
            list=iEmpleadoRepository.obtenerEmpleadoPorId(empAux);
            aux.setEstatus(true);
            aux.setMensaje("OK");

        }
        catch (Exception e){
            logger.error("ERROR obtenerEmpleadoPorId",e);
            list=null;
            aux.setEstatus(false);
            aux.setMensaje("No se pudo obtener el empleado");
            throw e;

        }
        logger.info("FIN obtenerEmpleadoPorId");
        aux.setEmpleados(list);
        return aux;
    }
    @Override
    public Contenedor actualizarEmpleado(Empleado data) {
        logger.info("INICIA actualizarEmpleado");
        Contenedor aux = new Contenedor();

        try {
            if(iEmpleadoRepository.actualizarEmpleado(data)){
                aux.setEstatus(true);
                aux.setMensaje("OK");
            }
            else{
                aux.setEstatus(false);
                aux.setMensaje("No se pudo actualizar el empleado");
            }

        }
        catch (Exception e){
            logger.error("ERROR actualizarEmpleado",e);

            aux.setEstatus(false);
            aux.setMensaje("No se pudo actualizar el empleado");
            throw e;
        }
        logger.info("FIN actualizarEmpleado");

        return aux;
    }
}
