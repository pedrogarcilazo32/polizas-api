package com.api.polizas.apipolizas.Services;

import com.api.polizas.apipolizas.Model.Contenedor;
import com.api.polizas.apipolizas.Model.Empleado;

import java.util.List;

public interface IEmpleadoServices {
    public Contenedor obtenerEmpleadosActivos();
    public  Contenedor obtenerEmpleadoPorId(int id);
    public Contenedor actualizarEmpleado(Empleado data);
}
