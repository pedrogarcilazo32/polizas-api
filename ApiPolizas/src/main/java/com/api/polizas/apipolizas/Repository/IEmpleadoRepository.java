package com.api.polizas.apipolizas.Repository;

import com.api.polizas.apipolizas.Model.Empleado;

import java.util.List;

public interface IEmpleadoRepository {
    public List<Empleado> obtenerEmpleadosActivos();
    public List<Empleado> obtenerEmpleadoPorId(Empleado data);
    public Boolean actualizarEmpleado(Empleado data);

}
