package com.api.polizas.apipolizas.Model;

import lombok.Data;

@Data
public class Inventario {
    String  sku_cod;
    String nombre;
    int cantidad;
    Boolean activo_opc;
    String fecha_reg;
}
