package com.api.polizas.apipolizas.Model;

import lombok.Data;

@Data
public class Puesto {
    int id;
    String nombre;
    Boolean activo_opc;
    String fecha_reg;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getActivo_opc() {
        return activo_opc;
    }

    public void setActivo_opc(Boolean activo_opc) {
        this.activo_opc = activo_opc;
    }

    public String getFecha_reg() {
        return fecha_reg;
    }

    public void setFecha_reg(String fecha_reg) {
        this.fecha_reg = fecha_reg;
    }
}
