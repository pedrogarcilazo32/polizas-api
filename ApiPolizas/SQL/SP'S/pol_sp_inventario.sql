USE [polizas]
GO
/****** Object:  StoredProcedure [dbo].[pol_sp_inventario]    Script Date: 26/08/2023 11:49:04 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[pol_sp_inventario]

@proceso int, @json VARCHAR(MAX)
AS 
    DECLARE
        @nombre VARCHAR(500),
        @id INT
--OBTENER EMPLEADOS ACTIVOS
    IF @proceso=5
    BEGIN
        SELECT
			sku_cod,
			nombre,
			cantidad
		FROM
			inventario
		WHERE
			activo_opc=1
       
    END

