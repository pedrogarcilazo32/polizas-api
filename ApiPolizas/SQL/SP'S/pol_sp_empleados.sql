USE [polizas]
GO
/****** Object:  StoredProcedure [dbo].[pol_sp_empleados]    Script Date: 26/08/2023 11:11:06 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[pol_sp_empleados]

@proceso int, @json VARCHAR(MAX)
AS 
    DECLARE
        @nombre VARCHAR(250),
		@apellido VARCHAR(250),
        @id INT,
		@estatus BIT,
		@mensaje VARCHAR(MAX)
		SET @estatus=0
		SET @mensaje=''
--ACTUALIZAR EMPLEADO 
    IF @proceso = 3
    BEGIN
      
        SET @id = JSON_VALUE(@json, '$.idEmpleado');
		SET @nombre= JSON_VALUE(@json,'$.nombre')
		SET @apellido= JSON_VALUE(@json,'$.apellido')
        BEGIN TRANSACTION;
        BEGIN TRY
		--ACTUALIZAR EL EMPLEADO
		  UPDATE 
			cat_empleado 
		  SET 
			nombre=@nombre,
			apellido=@apellido
		  WHERE
			idEmpleado=@id
		
		 SET @estatus=1
		 SET @mensaje='Emplado actualizado'
            COMMIT
        END TRY
        BEGIN CATCH
            SET @estatus=0
			SET @mensaje='No se pudo actualizar el empleado'
            ROLLBACK
        END CATCH
		SELECT @estatus as estatus,@mensaje as mensaje
    END
	
--OBTENER EMPLEADOS ACTIVOS
    IF @proceso=5
    BEGIN
        SELECT
		e.idEmpleado,
		e.nombre,
		e.apellido,
		e.idPuesto,
		e.fecha_reg,
		p.nombre as puestoNombre
        FROM 
            cat_empleado e
		INNER JOIN 
			cat_puesto p on p.idPuesto=e.idpuesto
		WHERE
			e.activo_opc=1
       
    END
	--OBTENER EMPLEADOS POR ID
    IF @proceso=6
    BEGIN
		SET @id=JSON_VALUE(@json,'$.idEmpleado')
        SELECT
		e.idEmpleado,
		e.nombre,
		e.apellido,
		e.idPuesto,
		e.fecha_reg,
		p.nombre as puestoNombre
        FROM 
            cat_empleado e
		INNER JOIN 
			cat_puesto p on p.idPuesto=e.idpuesto
		WHERE
			e.idEmpleado=@id
       
    END

