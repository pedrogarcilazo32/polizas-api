USE [polizas]
GO
/****** Object:  StoredProcedure [dbo].[pol_sp_puestos]    Script Date: 26/08/2023 11:49:15 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[pol_sp_puestos]

@proceso int, @json VARCHAR(MAX)
AS 
    DECLARE
        @nombre VARCHAR(500),
        @id INT
--OBTENER PUESTOS
    IF @proceso=2
    BEGIN
        SELECT
		idPuesto as id,
          nombre,
		  fecha_reg,
		  activo_opc
        FROM 
            cat_puesto
       
    END

