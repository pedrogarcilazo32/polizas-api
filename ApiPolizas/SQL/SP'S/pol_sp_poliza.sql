USE [polizas]
GO
/****** Object:  StoredProcedure [dbo].[pol_sp_poliza]    Script Date: 26/08/2023 08:53:39 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[pol_sp_poliza]
    @proceso INT,
    @json VARCHAR(MAX)
AS
BEGIN
    DECLARE
        @idEmpleado INT,
        @sku_cod VARCHAR(50),
        @cantidad INT,
        @rowCount INT,
        @cantidadAux INT,
        @fecha DATETIME = GETDATE(),
		@estatus BIT,
		@mensaje VARCHAR(MAX),
		@idPoliza INT,
		--PARA EL PAGINADO
		 @paginado INT,
          @pagina INT,         
          @busqueda VARCHAR(100),
          @total_paginas INT,
		  @total_elementos INT
		SET @estatus=0
		SET @mensaje=''
    IF @proceso = 1
    BEGIN
        -- Parsear los valores del JSON
        SET @idEmpleado = JSON_VALUE(@json, '$.idEmpleado');
        SET @sku_cod = JSON_VALUE(@json, '$.sku_cod');
        SET @cantidad = JSON_VALUE(@json, '$.cantidad');

        BEGIN TRANSACTION;

        BEGIN TRY
            -- Verificar si la cantidad disponible es suficiente
            SELECT
				@cantidadAux = cantidad 
			FROM 
				inventario WITH (UPDLOCK, ROWLOCK)
			WHERE 
				sku_cod = @sku_cod;

            IF @cantidad <= @cantidadAux
            BEGIN
                -- Actualizar el inventario y la tabla de polizas
                INSERT INTO cat_polizas
                VALUES 
				(
					@idEmpleado, @sku_cod, @cantidad, 1, @fecha
				)

                SET @rowCount = @@ROWCOUNT

                IF @rowCount > 0
                BEGIN
                    -- Actualizar la cantidad en la tabla de inventario
                    UPDATE 
						inventario
                    SET 
						cantidad = cantidad - @cantidad
                    WHERE 
						sku_cod = @sku_cod

                    SET @estatus=1
					SET @mensaje='Poliza generada'
                END
                ELSE
                BEGIN
                    SET @estatus=0
					SET @mensaje='No se pudo generar la poliza'
                END
            END
            ELSE
            BEGIN
               SET @estatus=0
			SET @mensaje='No se pudo generar la poliza por que la cantidad excedia a la cantidad total'
            END
			
            COMMIT
        END TRY
        BEGIN CATCH
            SET @estatus=0
			SET @mensaje='No se pudo generar la poliza'
            ROLLBACK
        END CATCH
		SELECT @estatus as estatus,@mensaje as mensaje
    END
	
END
--OBTENER POLIZAS
IF @proceso=2
BEGIN 
	
	SELECT 
		c.idPoliza,
		c.idEmpleado,
		c.sku_cod,
		c.fecha_reg,
		c.cantidad,
		c.activo_opc,
		e.nombre + ' ' +e.apellido as nombreEmpleado
	FROM 
		cat_polizas c
	INNER JOIN 
		cat_empleado e on e.idEmpleado=c.idEmpleado
	ORDER BY c.fecha_reg DESC

END
--DESACTIVAR POLIZA 
    IF @proceso = 4
    BEGIN
        -- Parsear los valores del JSON
        SET @idPoliza = JSON_VALUE(@json, '$.idPoliza');
      
        BEGIN TRANSACTION;

        BEGIN TRY
		--DESACTIVAR LA POLIZA
		  UPDATE 
			cat_polizas 
		  SET 
			activo_opc=0
		  WHERE
			idPoliza=@idPoliza
		-- LE DEVOLVEMOS SU CANTIDAD AL INVENTARIO
		SELECT 
			@sku_cod=sku_cod,
			@cantidad=cantidad
		FROM 
			cat_polizas
		WHERE
			idPoliza=@idPoliza

		UPDATE 
			Inventario
		SET
			cantidad=cantidad+@cantidad
		WHERE
			sku_cod=@sku_cod
		 SET @estatus=1
		SET @mensaje='Poliza eliminada'
            COMMIT
        END TRY
        BEGIN CATCH
            SET @estatus=0
			SET @mensaje='No se pudo eliminar la poliza'
            ROLLBACK
        END CATCH
		SELECT @estatus as estatus,@mensaje as mensaje
    END
	

--OBTENER POLIZAS PAGINADO
IF @proceso=5
BEGIN 
	SET @busqueda = JSON_VALUE(@json, '$.busqueda')
	SET @pagina = JSON_VALUE(@json, '$.pagina')
	SET @paginado = JSON_VALUE(@json, '$.paginado')
	IF @busqueda = '' BEGIN  SET @busqueda = NULL END
	SELECT 
		@total_elementos=COUNT(0)
	FROM 
		cat_polizas c
	INNER JOIN 
		cat_empleado e on e.idEmpleado=c.idEmpleado
	WHERE
		c.activo_opc=1
	AND
	(
		c.sku_cod LIKE COALESCE('%'+@busqueda+'%',c.sku_cod) 
	OR
		e.nombre LIKE COALESCE('%'+@busqueda+'%',e.nombre) 
	OR
		e.apellido  LIKE COALESCE('%'+@busqueda+'%',e.apellido) 
	)
	SELECT 
		c.idPoliza,
		c.idEmpleado,
		c.sku_cod,
		c.fecha_reg,
		c.cantidad,
		c.activo_opc,
		e.nombre + ' ' +e.apellido as nombreEmpleado,
		CASE WHEN CONVERT(VARCHAR(50),(CONVERT(int,@total_elementos ) / @paginado)) 
		LIKE '%.0%' THEN (( CONVERT(int,@total_elementos ) ) / @paginado ) 
		ELSE (( CONVERT(int,@total_elementos ) ) / @paginado ) + 1 END AS total_paginas
	FROM 
		cat_polizas c
	INNER JOIN 
		cat_empleado e on e.idEmpleado=c.idEmpleado
	WHERE
		c.activo_opc=1
	AND
	(
		c.sku_cod LIKE COALESCE('%'+@busqueda+'%',c.sku_cod) 
	OR
		e.nombre LIKE COALESCE('%'+@busqueda+'%',e.nombre) 
	OR
		e.apellido  LIKE COALESCE('%'+@busqueda+'%',e.apellido) 
	)

	ORDER BY c.fecha_reg DESC
	offset(@pagina-1)*@paginado ROWS 
	FETCH NEXT @paginado ROWS only
END
