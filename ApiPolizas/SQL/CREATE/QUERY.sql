
CREATE TABLE cat_puesto
(
	idPuesto INT IDENTITY PRIMARY KEY, 
	nombre VARCHAR(250) NOT NULL,
	activo_opc BIT  NOT NULL ,
	fecha_reg DATETIME  NOT NULL 

)
INSERT INTO cat_puesto VALUES('Supervisor regional',1,GETDATE())
INSERT INTO cat_puesto VALUES('Supervisor de area',1,GETDATE())
GO
CREATE TABLE cat_empleado
(
	idEmpleado INT IDENTITY PRIMARY KEY,
	nombre VARCHAR(250) NOT NULL,
	apellido VARCHAR(250) NOT NULL,
	idPuesto INT NOT NULL,
	activo_opc BIT NOT NULL,
	fecha_reg DATETIME NOT NULL
	CONSTRAINT FK_empleadoPuesto FOREIGN KEY (idPuesto) REFERENCES cat_puesto(idPuesto)
)
INSERT INTO cat_empleado VALUES('Pedro','Garcilazo',1,1,GETDATE())
INSERT INTO cat_empleado VALUES('Mirey','Garcilazo',1,1,GETDATE())
INSERT INTO cat_empleado VALUES('Mirey','Bernal',2,1,GETDATE())
INSERT INTO cat_empleado VALUES('Pedro','Bernal',2,1,GETDATE())

GO
CREATE TABLE Inventario
(
	sku_cod CHAR(3) PRIMARY KEY NOT NULL,
	nombre VARCHAR(250) NOT NULL,
	cantidad INT NOT NULL,
	activo_opc BIT NOT NULL,
	fecha_reg DATETIME NOT NULL
)
INSERT INTO Inventario VALUES ('LA1','Lapiz azul',100,1,GETDATE())
INSERT INTO Inventario VALUES ('LB1','Lapiz blanco',100,1,GETDATE())
INSERT INTO Inventario VALUES ('LV1','Lapiz verde',100,1,GETDATE())
INSERT INTO Inventario VALUES ('LA2','Lapiz amarillo',100,1,GETDATE())
INSERT INTO Inventario VALUES ('LN1','Lapiz negro',100,1,GETDATE())
GO
CREATE TABLE cat_polizas
(
	idPoliza INT IDENTITY PRIMARY KEY NOT NULL,
	idEmpleado INT NOT NULL,
	sku_cod CHAR(3) NOT NULL,
	cantidad INT NOT NULL,
	activo_opc BIT NOT NULL,
	fecha_reg DATETIME NOT NULL
	CONSTRAINT FK_polizaEmpleado FOREIGN KEY (idEmpleado) REFERENCES cat_empleado(idEmpleado),
	CONSTRAINT FK_polizaInventario FOREIGN KEY (sku_cod) REFERENCES Inventario(sku_cod)
)
GO